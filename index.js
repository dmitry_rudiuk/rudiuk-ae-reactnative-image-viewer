import { AppRegistry, YellowBox } from 'react-native'
import App from './src/App'

AppRegistry.registerComponent('AE500pxProject', () => App)

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'])
