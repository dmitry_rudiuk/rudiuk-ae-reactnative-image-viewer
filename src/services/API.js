// @flow
import fetch from 'node-fetch'
import { store } from '../App'
import * as storage from '../helpers/storage'

const API_KEY = '23567b218376f79d9415' // other valid API keys: '760b5fb497225856222a', '0e2a751704a65685eefc'
const API_ENDPOINT = 'http://195.39.233.28:8035'
const TOKEN_TYPE = 'Bearer'

const getAuthToken = () => {
  let { token } = store.getState().homeReducer
  console.log(token)
  return token
}

export const requestAuthToken = async () => {
  const token = await storage.get('token')

  if (!token) {
    const res = await fetch(`${API_ENDPOINT}/auth`, {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify({ apiKey: API_KEY }),
    })
    const json = await res.json()
    return json.token
  } else {
    return token
  }
}

const requestApi = async (url, options = {}) => {
  const token = getAuthToken()
  if (token) {
    const res = await fetch(`${API_ENDPOINT}${url}`, {
      headers: { Authorization: `${TOKEN_TYPE} ${token}` },
      method: options.method,
    })
    const json = await res.json()
    console.log(json)
    return json
  }
}

export async function getPictures (page: number = 1): Array<Object> {
  return requestApi(`/images?page=${page}`, {
    method: 'GET',
  })
}

export async function getPictureDetails (id: number): Object {
  return requestApi(`/images/${id}`, {
    method: 'GET',
  })
}
