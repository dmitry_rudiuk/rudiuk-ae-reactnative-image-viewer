import { AsyncStorage } from 'react-native'
const APP_NAME = 'AE500pxProject'

export const put = (key, value, callback) => AsyncStorage.setItem(`@${APP_NAME}:${key}`, value, callback)

export const get = key =>
  new Promise((resolve, reject) => {
    AsyncStorage.getItem(`@${APP_NAME}:${key}`, (error, result) => {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })

export const remove = (key, callback) => AsyncStorage.removeItem(`@${APP_NAME}:${key}`, callback)

export const clear = callback => AsyncStorage.clear(callback)
