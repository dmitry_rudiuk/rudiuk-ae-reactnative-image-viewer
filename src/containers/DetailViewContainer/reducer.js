import { PICTURE_DETAILS_FETCH_REQUESTED, PICTURE_DETAILS_FETCH_SUCCESS, PICTURE_DETAILS_FETCH_FAILED } from './actions'

const initialState = {
  hiResPictures: [],
  author: '',
  camera: '',
  cropped_picture: 'https://via.placeholder.com/300x300.png?text=Ups...+smthng+went+wrong',
  full_picture: 'https://via.placeholder.com/300x300.png?text=Ups...+smthng+went+wrong',
  isLoading: false,
  errorMessage: '',
}

export default function (state: any = initialState, action: Object) {
  const payload = action.payload
  switch (action.type) {
    case PICTURE_DETAILS_FETCH_REQUESTED:
      return {
        ...state,
        isLoading: true,
      }

    case PICTURE_DETAILS_FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        author: payload.author,
        camera: payload.camera,
        cropped_picture: payload.cropped_picture,
        full_picture: payload.full_picture,
      }
    case PICTURE_DETAILS_FETCH_FAILED:
      return {
        ...state,
        isLoading: false,
        errorMessage: payload.error,
      }
    default:
      return state
  }
}
