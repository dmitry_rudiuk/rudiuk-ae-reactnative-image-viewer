// @flow

import { getPictureDetails } from '../../services/API'
import { FETCH_FAILED } from '../HomeContainer/actions'
import type { ActionWithPayload, ActionWithoutPayload } from '../../types/actions'

export const PICTURE_DETAILS_FETCH_REQUESTED = 'PICTURE_DETAILS_FETCH_REQUESTED'
export const PICTURE_DETAILS_FETCH_SUCCESS = 'PICTURE_DETAILS_FETCH_SUCCESS'
export const PICTURE_DETAILS_FETCH_FAILED = 'PICTURE_DETAILS_FETCH_FAILED'

export function pictureIsLoading (): ActionWithoutPayload {
  return {
    type: PICTURE_DETAILS_FETCH_REQUESTED,
  }
}

export function fetchPictureSuccess (imageId: number, hiResImage: string): ActionWithPayload {
  return {
    // TODO: implement me
  }
}

export function fetchPictureFailed (errorMessage: string): ActionWithPayload {
  return {
    // TODO: implement me
  }
}

export const fetchPictureDetails = (imageId: number) => dispatch => {
  dispatch({
    type: PICTURE_DETAILS_FETCH_REQUESTED,
  })
  return getPictureDetails(imageId).then(
    response => {
      dispatch({
        type: PICTURE_DETAILS_FETCH_SUCCESS,
        payload: response,
      })
      return response
    },
    error => {
      dispatch({
        type: PICTURE_DETAILS_FETCH_FAILED,
        payload: {
          error,
        },
      })
    }
  )
}
