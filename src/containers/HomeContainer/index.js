// @flow
import * as React from 'react'
import { Platform, StatusBar } from 'react-native'
import { connect } from 'react-redux'

import HomeView from '../../screens/Home'
import { fetchPictures, login } from './actions'

export interface Props {
  navigation: any;
  fetchPictures: Function;
  login: Function;
  pictures: Array<Object>;
  isLoading: boolean;
  token: any;
}

export interface State {}

class HomeContainer extends React.Component<Props, State> {
  static navigationOptions = {
    header: null,
  }

  constructor (props) {
    super(props)
    StatusBar.setBarStyle('light-content')
    Platform.OS === 'android' && StatusBar.setBackgroundColor('#000')
    this.onLoadNext = this.onLoadNext.bind(this)
  }

  componentDidMount () {
    this.init()
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.token && nextProps.token !== this.props.token) {
      this.onLoadNext()
    }
  }

  init (): void {
    this.props.login()
  }

  onLoadNext (): void {
    const { page, pageCount } = this.props
    if (page < pageCount) {
      this.props.fetchPictures(page + 1)
    }
  }

  render () {
    return <HomeView {...this.props} onLoadNext={this.onLoadNext} />
  }
}

function bindAction (dispatch) {
  return {
    fetchPictures: page => dispatch(fetchPictures(page)),
    login: () => dispatch(login()),
  }
}

const mapStateToProps = state => ({
  token: state.homeReducer.token,
  pictures: state.homeReducer.pictures,
  page: state.homeReducer.page,
  pageCount: state.homeReducer.pageCount,
  isLoading: state.homeReducer.isLoading,
})

export default connect(
  mapStateToProps,
  bindAction
)(HomeContainer)
