import { getPictures, requestAuthToken } from '../../services/API'
import type { ActionWithPayload, ActionWithoutPayload } from '../../types/actions'
import * as storage from '../../helpers/storage'

export const PICTURES_FETCH_REQUESTED = 'PICTURES_FETCH_REQUESTED'
export const PICTURES_FETCH_SUCCESS = 'PICTURES_FETCH_SUCCESS'
export const FETCH_FAILED = 'FETCH_FAILED'
export const AUTH_SUCCESS = 'AUTH_SUCCESS'
export const AUTH_FAILED = 'AUTH_FAILED'

export function listIsLoading (): ActionWithoutPayload {
  return {
    type: PICTURES_FETCH_REQUESTED,
  }
}

export function fetchListSuccess (pictures: Array<Object>, page: number): ActionWithPayload {
  return {
    // TODO: implement me
  }
}

export function fetchListFailed (errorMessage: string): ActionWithPayload {
  return {
    // TODO: implement me
  }
}

export const fetchPictures = (page: number = 1) => dispatch => {
  dispatch({
    type: PICTURES_FETCH_REQUESTED,
  })
  return getPictures(page).then(
    response => {
      dispatch({
        type: PICTURES_FETCH_SUCCESS,
        payload: response,
      })
      return response
    },
    error => {
      dispatch({
        type: FETCH_FAILED,
        payload: {
          error,
        },
      })
    }
  )
}

export const login = (): ActionWithPayload => dispatch => {
  return requestAuthToken().then(
    response => {
      dispatch({
        type: AUTH_SUCCESS,
        payload: {
          token: response,
        },
      })
      storage.put('token', response)
      return true
    },
    error => {
      dispatch({
        type: AUTH_FAILED,
        payload: {
          error,
        },
      })
    }
  )
}
