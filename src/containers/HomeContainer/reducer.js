// @flow
import { AUTH_SUCCESS, AUTH_FAILED, FETCH_FAILED, PICTURES_FETCH_SUCCESS, PICTURES_FETCH_REQUESTED } from './actions'
const initialState = {
  token: null,
  pictures: [],
  isLoading: false,
  page: 0,
  pageCount: 1,
  errorMessage: '',
}

const groupPictures = (payload, state) => {
  const arr = state.pictures
  payload.pictures.map(item => {
    arr.push(item)
  })
  return arr
}

export default function (state: any = initialState, action: Object) {
  const payload = action.payload
  switch (action.type) {
    case AUTH_SUCCESS:
      return {
        ...state,
        token: payload.token,
      }

    case AUTH_FAILED:
      return {
        ...state,
        token: null,
        errorMessage: payload.error,
      }

    case PICTURES_FETCH_REQUESTED:
      return {
        ...state,
        isLoading: true,
      }

    case PICTURES_FETCH_SUCCESS:
      return {
        ...state,
        pictures: groupPictures(payload, state),
        page: payload.page,
        pageCount: payload.pageCount,
        isLoading: false,
      }

    case FETCH_FAILED:
      return {
        ...state,
        isLoading: false,
        errorMessage: payload.error,
      }
    default:
      return state
  }
}
