// @flow
import * as React from 'react'
import { View, Image, Dimensions } from 'react-native'

import styles from './styles'
import DetailsFooter from './components/DetailsFooter'
import ImageZoom from 'react-native-image-pan-zoom'

type Props = {
  imageUrl: string,
  author: string,
  camera: string,
  isLoading: boolean,
  shareCallback: Function,
  applyFilterCallback: Function,
  pictureDetails: Object,
}

// TODO: it would be great to see here loader, pinch to zoom here and pan

class DetailView extends React.PureComponent<Props> {
  render () {
    const { imageUrl, isLoading, shareCallback, applyFilterCallback, pictureDetails, author, camera } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <ImageZoom
            cropWidth={Dimensions.get('window').width}
            cropHeight={Dimensions.get('window').height}
            imageWidth={Dimensions.get('window').width * 0.9}
            imageHeight={Dimensions.get('window').width * 0.9}
            maxScale={5}
          >
            <Image source={{ uri: imageUrl }} style={styles.imageStyle} />
          </ImageZoom>
        </View>
        <DetailsFooter
          author={author}
          camera={camera}
          imageUrl={imageUrl}
          pictureDetails={pictureDetails}
          shareCallback={shareCallback}
          applyFilterCallback={applyFilterCallback}
        />
      </View>
    )
  }
}

export default DetailView
