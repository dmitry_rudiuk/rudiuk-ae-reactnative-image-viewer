import * as React from 'react'
import { TouchableOpacity, Image, View, Text } from 'react-native'
import styles from '../styles'
import imageFiltersImage from './images/ImageFilters.png'
import shareImage from './images/ShareThis.png'

type Props = {
  shareCallback: Function,
  applyFilterCallback: Function,
  pictureDetails: Object,
  author: string,
  camera: string,
  imageUrl: string,
}

class DetailsFooter extends React.PureComponent<Props> {
  render () {
    const { imageUrl, shareCallback, applyFilterCallback, pictureDetails, camera, author } = this.props
    if (!pictureDetails) return null

    return (
      <View style={styles.footerView}>
        <View style={styles.detailTextView}>
          <Text style={styles.detailText}>{author}</Text>
          <Text style={styles.detailText}>{camera}</Text>
        </View>
        <View style={styles.detailView}>
          <TouchableOpacity style={{ marginRight: 10 }} onPress={() => applyFilterCallback()}>
            <Image style={styles.detailViewImage} resizeMode='cover' source={imageFiltersImage} />
          </TouchableOpacity>
          <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => shareCallback(imageUrl)}>
            <Image style={styles.detailViewImage} resizeMode='cover' source={shareImage} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default DetailsFooter
